using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ViewerController : MonoBehaviour
{
    [SerializeField]
    private Camera m_puzzleCamera;

    [SerializeField]
    private Transform m_puzzleCameraParent;

    [SerializeField]
    private GameObject m_viewerPanel;

    [SerializeField]
    private PuzzleSettings m_settings;

    [SerializeField]
    private float m_camRotationSpeed;

    [SerializeField]
    private float m_camZoomSpeed;

    private Vector2 mousePos;
    private bool mouseOverViewer;
    private Vector3 camEuler;
    private float camFOV;
    private float baseCamFOV;

    private void Start()
    {
        baseCamFOV = m_puzzleCamera.fieldOfView;
        camFOV = baseCamFOV;
    }
    public void Enable(bool active)
    {
        m_viewerPanel.SetActive(active);
    }

    public void CenterCameraOnContent(PuzzleData puzzleData)
    {
        // get bounds
        Vector3 center = new Vector3((puzzleData.rows - 1) * m_settings.PieceSpacing / 2, 0, (puzzleData.cols - 1) * m_settings.PieceSpacing / 2);
        Vector3 extent = center + new Vector3(m_settings.PieceSpacing / 4, 0, m_settings.PieceSpacing / 4);
        m_puzzleCamera.fieldOfView = baseCamFOV;

        // center camera on center
        m_puzzleCameraParent.transform.eulerAngles = Vector3.zero;
        m_puzzleCameraParent.transform.position = center;

        // Move camera up to see everything
        float maxExtent = extent.magnitude;
        float minDistance = (maxExtent) / Mathf.Sin(Mathf.Deg2Rad * m_puzzleCamera.fieldOfView / 2.0f);
        m_puzzleCamera.transform.localPosition = Vector3.up * minDistance;
    }

    void Update()
    {
        // Rotation
        if (Input.GetMouseButtonDown(0))
        {
            mouseOverViewer = MouseOnViewer();
        }
        if (Input.GetMouseButtonUp(0))
        {
            mouseOverViewer = false;
        }
        if (Input.GetMouseButton(0) && mouseOverViewer)
        {
            m_puzzleCameraParent.transform.eulerAngles += m_camRotationSpeed * new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0);
        }

        // Zoom
        float ScrollWheelChange = -Input.GetAxis("Mouse ScrollWheel");
        if (ScrollWheelChange!=0) {
            if (MouseOnViewer())
            {
                camFOV += ScrollWheelChange * m_camZoomSpeed;
                camFOV = Mathf.Clamp(camFOV, 15, 90);
                m_puzzleCamera.fieldOfView = camFOV;
            }
        }
    }

    private bool MouseOnViewer()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

            foreach(var res in results)
            {
                if(res.gameObject.layer == LayerMask.NameToLayer("Viewer"))
                {
                    return true;
                }
            }
        }
        return false;
    }
}