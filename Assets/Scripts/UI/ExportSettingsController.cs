using SFB;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ExportSettingsController : MonoBehaviour
{
    [Header("Managers")]
    [SerializeField]
    private PuzzleGenerator m_puzzleGenerator;

    [SerializeField]
    private PuzzleExporter m_puzzleExporter;

    [Header("UI elements")]
    [SerializeField]
    private Button m_selectFolderBtn;

    [SerializeField]
    private TextMeshProUGUI m_folderPath;

    [SerializeField]
    private Toggle m_individualExport;

    [SerializeField]
    private Button m_ExportBtn;

    public string folderPath { get; private set; }
    private void Start()
    {
        folderPath = string.Empty;
        m_selectFolderBtn.onClick.AddListener(OpenFolderPanel);
        m_ExportBtn.onClick.AddListener(ExportSelection);
    }

    private void OnDestroy()
    {
        m_selectFolderBtn.onClick.RemoveListener(OpenFolderPanel);
        m_ExportBtn.onClick.RemoveListener(ExportSelection);
    }

    private void OpenFolderPanel()
    {
        var path = StandaloneFileBrowser.OpenFolderPanel("Select a folder", "", false);
        if (path.Length > 0)
        {
            folderPath = path[0];
            m_folderPath.text = Path.GetFileName(folderPath);
            if (m_puzzleGenerator.GeneratedPieces != null && m_puzzleGenerator.GeneratedPieces.Count > 0)
            {
                m_ExportBtn.interactable = true;
            }
        }
    }

    private void ExportSelection()
    {
        if (!string.IsNullOrEmpty(folderPath))
        {
            m_puzzleExporter.ExportObjects(folderPath, m_individualExport.isOn);
        }
    }
}
