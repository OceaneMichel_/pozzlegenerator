using TMPro;
using UnityEngine;

public class InputFieldFloatRangeSetter : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField m_inputField;

    [SerializeField]
    private float m_minVal;

    [SerializeField]
    private float m_maxVal;

    [SerializeField]
    private float m_baseVal;

    private void Start()
    {
        m_inputField.onEndEdit.AddListener(SetValueRange);
        m_baseVal = float.Parse(m_inputField.text);
    }
    private void OnDestroy()
    {
        m_inputField.onEndEdit.RemoveListener(SetValueRange);
    }

    private void SetValueRange(string val)
    {
        float enteredValue;
        if (float.TryParse(val, out enteredValue))
        {
            if (enteredValue < m_minVal)
            {
                m_inputField.text = m_minVal.ToString();
            }
            else if (enteredValue > m_maxVal)
            {
                m_inputField.text = m_maxVal.ToString();
            }
        }
        else
        {
            m_inputField.text = m_baseVal.ToString();
        }
    }
}
