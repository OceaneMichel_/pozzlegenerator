using SFB;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GenerationSettingsController : MonoBehaviour
{
    [Header("Managers")]
    [SerializeField]
    private PuzzleGenerator m_puzzleGenerator;

    [SerializeField]
    private PuzzleSettings m_puzzleSettings;

    [SerializeField]
    private ViewerController m_viewer;

    [Header("UI elements")]
    [SerializeField]
    private GameObject m_exportSection;

    [SerializeField]
    private Button m_selectFileBtn;

    [SerializeField]
    private TextMeshProUGUI m_filePath;

    [SerializeField]
    private TMP_InputField m_pieceHeight;

    [SerializeField]
    private TMP_InputField m_pieceSpacing;

    [SerializeField]
    private Toggle m_randomPlacement;

    [SerializeField]
    private Button m_GenerateBtn;

    private string filePath;

    private void Start()
    {
        m_selectFileBtn.onClick.AddListener(OpenFilePanel);
        m_GenerateBtn.onClick.AddListener(GeneratePuzzle);
    }

    private void OnDestroy()
    {
        m_selectFileBtn.onClick.RemoveListener(OpenFilePanel);
        m_GenerateBtn.onClick.RemoveListener(GeneratePuzzle);
    }

    private void OpenFilePanel()
    {
        var extensions = new[] { new ExtensionFilter("Puzzle configuration", "json") };
        var path = StandaloneFileBrowser.OpenFilePanel("Open File", "", extensions, false);
        if (path.Length > 0)
        {
            filePath = path[0];
            m_filePath.text = Path.GetFileName(filePath);
            m_GenerateBtn.interactable = true;
        }
    }

    private void GeneratePuzzle()
    {
        m_puzzleSettings.PieceHeight = float.Parse(m_pieceHeight.text) / 10;
        m_puzzleSettings.PieceSpacing = float.Parse(m_pieceSpacing.text);
        m_puzzleSettings.RandomPlacement = m_randomPlacement.isOn;
        if (m_puzzleGenerator.GenerateFromJSON(filePath))
        {
            m_exportSection.SetActive(true);
            m_viewer.Enable(true);
            m_viewer.CenterCameraOnContent(m_puzzleGenerator.PuzzleData);
        }
        else
        {
            m_exportSection.SetActive(false);
            m_viewer.Enable(false);
        }
    }
}
