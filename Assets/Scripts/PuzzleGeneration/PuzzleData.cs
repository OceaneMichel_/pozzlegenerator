using System;
using UnityEngine;

[Serializable]
public class PuzzleData
{
    public int rows;
    public int cols;
    public PieceData[] pieces;

    public static PuzzleData CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<PuzzleData>(jsonString);
    }

    [Serializable]
    public class PieceData
    {
        public int top;
        public int right;
        public int bottom;
        public int left;
    }
}
