using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using static PuzzleData;

public class PuzzleGenerator : MonoBehaviour
{
    [SerializeField]
    private PuzzleSettings m_settings;

    [SerializeField]
    private PieceVisual m_piecePrefab;

    [SerializeField]
    private float m_sizeFactor;

    [Header("JSON Based")]
    [SerializeField]
    private string m_puzzleJsonPath;

    [Header("String based")]
    [SerializeField]
    private string m_pieceConfiguration;

    public List<GameObject> GeneratedPieces { get; private set; }
    public GameObject PieceParentGameObject { get; private set; }
    public PuzzleData PuzzleData { get; private set; }

    [ContextMenu("Generate puzzle from JSON")]
    public bool GenerateFromJSON()
    {
        ClearPuzzle();
        GeneratedPieces = new List<GameObject>();

        StreamReader reader = new StreamReader(m_puzzleJsonPath);
        string jsonString = reader.ReadToEnd();
        PuzzleData = PuzzleData.CreateFromJSON(jsonString);
        Debug.Log(jsonString);
        reader.Close();

        // Shuffle puzzle data pieces
        if (m_settings.RandomPlacement)
        {
            System.Random rnd = new System.Random();
            PieceData[] randomPiecesArray = PuzzleData.pieces.OrderBy(x => rnd.Next()).ToArray();
            PuzzleData.pieces = randomPiecesArray;
        }

        PieceParentGameObject = new GameObject("Puzzle");
        for (int i = 0; i < PuzzleData.rows; i++)
        {
            for (int j = 0; j < PuzzleData.cols; j++)
            {
                // 1 chance out of 2 to flip the piece
                System.Random rand = new System.Random();
                bool flipPiece = rand.Next(0, 2) == 0;

                var pieceObject = GeneratePiece(PuzzleData.pieces[j + i * PuzzleData.cols], flipPiece);
                if (pieceObject)
                {
                    pieceObject.transform.localScale = new Vector3(1, m_settings.PieceHeight, 1);

                    // combine mesh
                    CombineMeshInPiece(pieceObject);
                    pieceObject.transform.localScale = Vector3.one * m_sizeFactor;
                    pieceObject.position = new Vector3(m_settings.PieceSpacing * i, 0, m_settings.PieceSpacing * j);
                    GeneratedPieces.Add(pieceObject.gameObject);
                }
                else
                {
                    return false;
                }
            }
        }

        return true;
    }

    public bool GenerateFromJSON(string jsonPath)
    {
        m_puzzleJsonPath = jsonPath;
        return GenerateFromJSON();
    }

    public void ClearPuzzle()
    {
        if (GeneratedPieces != null && GeneratedPieces.Count > 0)
        {
            DestroyImmediate(PieceParentGameObject);
            GeneratedPieces.Clear();
        }
    }

    [ContextMenu("Generate example piece")]
    public void GenerateExamplePiece()
    {
        var basePiece = GeneratePiece(m_pieceConfiguration);
        if (basePiece)
        {
            basePiece.transform.localScale = new Vector3(1, m_settings.PieceHeight, 1);

            // combine mesh
            CombineMeshInPiece(basePiece);
            basePiece.transform.localScale = Vector3.one;
        }
    }

    public Transform GeneratePiece(PieceData piece, bool flip = false)
    {
        var topObj = GetTipObjectFromConf(piece.top);
        var rightObj = GetTipObjectFromConf(flip ? piece.right : piece.left);
        var bottomObj = GetTipObjectFromConf(piece.bottom);
        var leftObj = GetTipObjectFromConf(flip ? piece.left : piece.right);

        if (topObj && rightObj && bottomObj && leftObj)
        {
            var puzzlePieceTrans = Instantiate(m_piecePrefab, PieceParentGameObject.transform);
            Instantiate(topObj, puzzlePieceTrans.TopTransform);
            Instantiate(rightObj, puzzlePieceTrans.RightTransform);
            Instantiate(bottomObj, puzzlePieceTrans.BottomTransform);
            Instantiate(leftObj, puzzlePieceTrans.LeftTransform);
            return puzzlePieceTrans.transform;
        }
        else
        {
            return null;
        }
    }

    private GameObject GetTipObjectFromConf(int tipConf)
    {
        var tipIndex = Math.Abs(tipConf);
        if (tipIndex - 1 >= m_settings.TipsConfigurations.Length || tipIndex < 1)
        {
            Debug.LogWarning("Invalid tip configuration. Abort piece generation.");
            return null;
        }
        var tipObj = tipConf < 0 ? m_settings.TipsConfigurations[tipIndex - 1].Hollow : m_settings.TipsConfigurations[tipIndex - 1].Full;
        return tipObj;
    }

    public Transform GeneratePiece(string pieceConfig)
    {
        // Parse piece config
        var tipsConfigs = pieceConfig.Split(" ");
        if (tipsConfigs.Length != 4)
        {
            Debug.LogWarning(string.Format("Required 4 tips configuration, but found {0}. Abort piece generation.", tipsConfigs.Length));
            return null;
        }

        List<GameObject> tipObjectsToGenerate = new List<GameObject>();
        for (int i = 0; i < tipsConfigs.Length; i++)
        {
            int tipConfig = Int32.Parse(tipsConfigs[i]);
            int tipIndex = Math.Abs(tipConfig);
            if (tipIndex - 1 > tipsConfigs.Length || tipIndex < 1)
            {
                Debug.LogWarning("Invalid tip configuration. Abort piece generation.");
                return null;
            }
            tipObjectsToGenerate.Add(tipConfig < 0 ? m_settings.TipsConfigurations[tipIndex - 1].Hollow : m_settings.TipsConfigurations[tipIndex - 1].Full);
        }

        var puzzlePieceTrans = Instantiate(m_piecePrefab);
        Instantiate(tipObjectsToGenerate[0], puzzlePieceTrans.TopTransform);
        Instantiate(tipObjectsToGenerate[1], puzzlePieceTrans.RightTransform);
        Instantiate(tipObjectsToGenerate[2], puzzlePieceTrans.BottomTransform);
        Instantiate(tipObjectsToGenerate[3], puzzlePieceTrans.LeftTransform);

        return puzzlePieceTrans.transform;
    }

    private void CombineMeshInPiece(Transform tempPiece)
    {
        MeshFilter[] meshFilters = tempPiece.GetComponentsInChildren<MeshFilter>();
        CombineInstance[] combine = new CombineInstance[meshFilters.Length];

        int i = 0;
        while (i < meshFilters.Length)
        {
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
            DestroyImmediate(meshFilters[i].gameObject);//.SetActive(false);

            i++;
        }

        var meshFilter = tempPiece.gameObject.AddComponent<MeshFilter>();
        meshFilter.sharedMesh = new Mesh();
        meshFilter.sharedMesh.CombineMeshes(combine);
    }
}
