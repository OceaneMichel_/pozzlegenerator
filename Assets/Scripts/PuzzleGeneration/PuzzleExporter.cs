using Parabox.Stl;
using UnityEngine;

public class PuzzleExporter : MonoBehaviour
{
    [SerializeField]
    private PuzzleGenerator m_generator;

    [ContextMenu("Export")]
    public void ExportObjects(string exportPath, bool individualExport)
    {
        string fileName;
        if (individualExport)
        { 
            var piecesArray =  m_generator.GeneratedPieces.ToArray();
            for (int i=0; i< piecesArray.Length; i++)
            {
                fileName = exportPath + string.Format(@"\Piece{0}.stl", (i + 1));
                Exporter.Export(fileName, new GameObject[] { piecesArray[i] }, FileType.Binary);
            }
        }
        else
        {
            fileName = exportPath + @"\Pozzle.stl";
            Exporter.Export(fileName, m_generator.GeneratedPieces.ToArray(), FileType.Binary);
        }
    }
}
