using UnityEngine;

public class PieceVisual : MonoBehaviour
{
    [SerializeField]
    private Transform topTransform;
    public Transform TopTransform { get { return topTransform; } }

    [SerializeField]
    private Transform rightTransform;
    public Transform RightTransform { get { return rightTransform; } }

    [SerializeField]
    private Transform bottomTransform;
    public Transform BottomTransform { get { return bottomTransform; } }

    [SerializeField]
    private Transform leftTransform;
    public Transform LeftTransform { get { return leftTransform; } }


}
