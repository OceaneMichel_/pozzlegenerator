using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Puzzle Settings", menuName = "ScriptableObjects/PuzzleSettings", order = 1)]

public class PuzzleSettings : ScriptableObject
{
    public float PieceHeight;
    public float PieceSpacing;
    public bool RandomPlacement;

    public GameObject MiddlePart;

    public Tip[] TipsConfigurations;

    [Serializable]
    public struct Tip
    {
        public GameObject Full;
        public GameObject Hollow;
    }
}
